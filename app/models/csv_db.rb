require 'csv'
class CsvDb
  def self.parse_data(target_model, csv_file, csv_options, &block)
    csv_options.reverse_merge!({ col_sep: ';',  encoding: 'UFT-8' })
    csv_data = CSV.parse(csv_file.read, csv_options)
    headers = csv_data.shift.map { |column| column.downcase.gsub(/\s+/, '_') }
    csv_data.each do |raw|
      yield(target_model, Hash[headers.zip(raw)]) if raw.present?
    end
  end
end
